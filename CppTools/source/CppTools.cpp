/* CppTools.cpp: Tcl initializacion

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include <tcl.h>

//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_init.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bsetgra.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bdir.h>
#include <ltdl.h>

static int _StatusLoadTcl = 0;
static BText TOLDirectory;

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

static
int _InitLocalTclStubs_( lt_dlhandle h );
static int _InitTclInterpreter( );

#define STRFY(a) #a
#if defined(WIN32)
#define TCL_LIBNAME(h,l) "tcl" STRFY(h) STRFY(l)  ".dll";
#else
#define TCL_LIBNAME(h,l) "libtcl" STRFY(h) "." STRFY(l) ".so"
#endif

static
lt_dlhandle LoadTclLibrary()
{
  lt_dlhandle h;
  const char *nameFromTclH = TCL_LIBNAME(TCL_MAJOR_VERSION, TCL_MINOR_VERSION);
  h = lt_dlopen( nameFromTclH );
  if( h )
    {
    return h;
    }

#if defined(WIN32)
  const char *hints[3];
  //BText tolBinPath = GetFilePath( GetPathTolDll( ) );
  BText tolBinPath = TOLDirectory + "/bin/";
  BText hints0 = tolBinPath + TCL_LIBNAME(8,6);
  BText hints1 = tolBinPath + TCL_LIBNAME(8,5);
  BText hints2 = tolBinPath + TCL_LIBNAME(8,4);
  hints[0] = hints0.Buffer( );
  hints[1] = hints1.Buffer( );
  hints[2] = hints2.Buffer( );  
#else
  const char *hints[] =
    {
      TCL_LIBNAME(8,6),
      TCL_LIBNAME(8,5),
      TCL_LIBNAME(8,4),
    };
#endif
  for( int i = 0; i < sizeof(hints)/sizeof(hints[0]); i++ )
    {
    //std::cout << "trying " <<  hints[i] << std::endl;
    h = lt_dlopen( hints[i] );
    if( h ) 
      {
      return h;
      }
    }
  return NULL;
}

static
BText GetParentPath( BText path )
{
  BText dir = GetFilePath( path );
  if ( dir.Last() == '/' )
    {
    dir = dir.SubString( 0, dir.Length() - 2 );
    }
  return dir;
}

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function 
DynAPI void* GetDynLibNameBlockTclCore()
{
  //const char *coreLibName = TCL_LIBNAME(TCL_MAJOR_VERSION, TCL_MINOR_VERSION);
  //printf( "coreLibName = %s\n", coreLibName );
  const BSetFromFile* firstIncluded = BSetFromFile::FindCompiled( BSetFromFile::NSetFromFileGlobal() );
  if ( firstIncluded )
    {
    // std::cout << "firstIncluded = " << firstIncluded->TolPath() << std::endl;
    BText stdlib_tol = firstIncluded->TolPath();
    BText stdlib_dir = GetParentPath( stdlib_tol );
    BText bin_dir = GetParentPath( stdlib_dir );
    TOLDirectory = GetParentPath( bin_dir );
    // std::cout << "TOLDirectory = " << TOLDirectory.Buffer() << std::endl;
    lt_dlhandle h = LoadTclLibrary();
    if( !h )
      {
      Error( BText( I2("Unable to load the tcl library",
                       "No se pudo cargar la libreria de tcl ") ) );
      return 0;
      }
    _StatusLoadTcl = _InitLocalTclStubs_( h );
    if( _StatusLoadTcl )
      {
      _StatusLoadTcl =_InitTclInterpreter();
      }
    }
  else
    {
    Error( BText( I2( "TolCore has not been loaded, this must be an invalid TOL instalation",
                      "TolCore no ha sido cargado, esto debe ser una instalación inválida de TOL") ) );
    
    }
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

static Tcl_Interp* _DefaultInterp = NULL;

typedef void (*pTcl_SetDefaultEncodingDir)(const char*);
typedef void (*pTcl_FindExecutable)(const char * argv0);
typedef Tcl_Interp * (*pTcl_CreateInterp)(void);
typedef const char * (*pTcl_SetVar)(Tcl_Interp * interp, const char * varName, const char * newValue, int flags);
typedef const char * (*pTcl_SetVar2)(Tcl_Interp * interp, const char * part1, const char * part2, const char * newValue, int flags);
typedef int (*pTcl_Init)(Tcl_Interp * interp);
typedef int (*pTcl_Eval)(Tcl_Interp * interp, const char * string);
typedef const char * (*pTcl_GetStringResult)(Tcl_Interp * interp);

static pTcl_SetDefaultEncodingDir tcl_SetDefaultEncodingDir = NULL;
static pTcl_FindExecutable tcl_FindExecutable = NULL;
static pTcl_CreateInterp tcl_CreateInterp = NULL;
static pTcl_SetVar tcl_SetVar = NULL;
static pTcl_SetVar2 tcl_SetVar2 = NULL;
static pTcl_Init tcl_Init = NULL;
static pTcl_Eval tcl_Eval = NULL;
static pTcl_GetStringResult tcl_GetStringResult = NULL;

template< class T>
int LoadTclSymbol( lt_dlhandle h, T &ptr, const char *name )
{
  ptr = (T)lt_dlsym( h, name );
  if (!ptr)
    {
    Error( BText( "No se pudo cargar el símbolo " ) << name );
    return 0;
    }
  return 1;
}

static int _InitLocalTclStubs_( lt_dlhandle h )
{
  if (!LoadTclSymbol( h, tcl_SetDefaultEncodingDir, "Tcl_SetDefaultEncodingDir" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_FindExecutable, "Tcl_FindExecutable" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_CreateInterp, "Tcl_CreateInterp" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_SetVar, "Tcl_SetVar" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_SetVar2, "Tcl_SetVar2" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_Init, "Tcl_Init" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_Eval, "Tcl_Eval" ) )
    {
    return 0;
    }
  if (!LoadTclSymbol( h, tcl_GetStringResult, "Tcl_GetStringResult" ) )
    {
    return 0;
    }
  return 1;
};

static int _InitTclInterpreter( )
{
  tcl_FindExecutable( TOLSessionPath() );
  _DefaultInterp = tcl_CreateInterp( );
  if ( tcl_Init( _DefaultInterp ) != TCL_OK ) 
    {
    Error( tcl_GetStringResult( _DefaultInterp ) );
    return 0;
    }
  return 1;
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatGetCoreLibStatus);
DefMethod(1, BDatGetCoreLibStatus, 
  "GetCoreLibStatus", 1, 1, "Real",
  "(Real void)",
  "True if tcl core lib could be loaded False in other case",
  BOperClassify::System_);
void BDatGetCoreLibStatus::CalcContens()
//--------------------------------------------------------------------
{
  contens_ = _StatusLoadTcl;
}

//--------------------------------------------------------------------
DeclareContensClass(BText, BTxtTemporary, BTxtGetTOLDirectory);
DefMethod(1, BTxtGetTOLDirectory, 
  "GetTOLDirectory", 1, 1, "Real",
  "(Real void)",
  "Return the computed TOL root directory based on stdlib",
  BOperClassify::System_);
void BTxtGetTOLDirectory::CalcContens()
//--------------------------------------------------------------------
{
  contens_ = TOLDirectory;
}

//--------------------------------------------------------------------
DeclareContensClass( BSet, BSetTemporary, BSetEval );
DefMethod(1, BSetEval, 
  "Eval", 1, 1, "Text",
  "(Text TclScript)",
  "Evaluate a tcl script",
  BOperClassify::System_);
void BSetEval::CalcContens()
//--------------------------------------------------------------------
{
  BText &script = Text( Arg( 1 ) );
  BDat status;
  BText result;

  int _status = tcl_Eval( _DefaultInterp, script );
  if ( _status == TCL_OK ) {
    status = 1.0;
  } else {
    status = 0.0;
  }

  BUserText* txtResult = new BContensText( "",
                                           tcl_GetStringResult( _DefaultInterp ),
                                           "");
  txtResult->PutName( "result" );

  BUserDat* datStatus = new BContensDat( "", status, "" );
  datStatus->PutName( "status" );

  BList *members = Cons( datStatus, Cons( txtResult, NIL ) );
  contens_.RobElement( members );
}
